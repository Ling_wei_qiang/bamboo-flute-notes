## 竹笛 —— 五调选择

五调 分别为 C D E F G

## 直观区别

C/D/E/F/G调依次(大到小)变小变细，C调最大，对气息要求也最多，因为C调内部空间最大，可以存放气息最多。  
如果所吹奏的气息，不足以通过音孔出来，那么就吹不响音。

`初学不推C调`有两点:

1. 气息不足，肺活量不够
2. 气息足，但吹进去的气息较少，多数漏外，也就难以吹响。

对于成年人: 肺活量及手指普遍比较适应，**首选D调**  
个人喜欢欢快曲风可选**F调** -- 喜欢低沉之感，肺量ok，C调可选

长期而言,F

## C

## D 

## E

## F

## G
